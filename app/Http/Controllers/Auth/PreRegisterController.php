<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PreRegisterController extends Controller
{
    protected function preregister($guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/');
        }else{
            return view('auth.preregister');
        }
    }

    protected function check(Request $request)
    {

        if ($request['check']){
            return redirect('register');
        } else{
            return view('auth.preregisterfail');
        }
    }
}