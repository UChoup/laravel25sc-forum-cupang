<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Auth;
use File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getAll(){
        $postlist = Post::all();

        return view('blog', compact('postlist'));
    }

    public function editAll($id)
    {
        $post = Post::find($id);
        // dd($postlist);
        return view('details', compact('post'));
    }

    public function index()
    {
        // $post = Post::all();
        $posts = Auth::user();
        $post = $posts->posts;
        return view('templateuser.posting.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('templateuser.posting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "title" => 'required|unique:posts',
            "body" => 'required',
            "img" => 'required|mimes:png,jpg,jpeg|max:5000'
        ]);

        $gambar = $request->img;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();

        $post = Post::create([
            "title" => $request->title,
            "body" => $request->body,
            "photo" => $new_gambar,
            "user_id" => Auth::id()
        ]);

        $gambar->move('uploads/posting/', $new_gambar);
        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('templateuser.posting.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('templateuser.posting.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gambar = $request->img;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
        $update = Post::where('id', $id)->update([
            "title" => $request->title,
            "body" => $request->body,
            "photo" => $new_gambar
        ]);

        $gambar->move('uploads/posting/', $new_gambar);
        return redirect('/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return redirect('/post');
    }
    public function all() {
        $posts = Auth::user();
        $post = $posts->posts;
        return view('/blog', compact('post'));
    }

    public function detail($id) {
        $post = Post::find($id);
        return view('/details', compact('post'));
    }
}