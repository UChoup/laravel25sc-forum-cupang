<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;

class PdfController extends Controller
{
    public function generatePdf(){
        $pdf = PDF::loadView('pdf.terms');
        // return $pdf->download('terms and condition.pdf');
        return $pdf->stream();
    }
}
