<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use App\User;
use Auth;
use File;

class profilController extends Controller
{
    public function show($id){

        // menggunakan model
        $post = User::find($id);
        return view('layouts.profile', compact('post'));
    }
     public function check(){
        $query = Profile::where('user_id', Auth::id())->first();
        if ($query == null) {
            return view('layouts.lengkapiprofile');
        } else {
            return view('layouts.profile');
        }

    }

    public function store(Request $request){

        $gambar = $request->img;
        $new_gambar = time() . ' . ' . $gambar->getClientOriginalName();

        $post = Profile::create([
            "phone" => $request["phone"],
            "gender" => $request["gender"],
            'photo' => $new_gambar,
            "user_id" => Auth::id()

        ]);

        $gambar->move('uploads/posts/',$new_gambar);
        return redirect('/user/profil');
    }

    public function edit(){
        return view('layouts.editprofile');
    }


    public function update($id, Request $request){
        $gambar = $request->img;
        $new_gambar = time() . ' . ' . $gambar->getClientOriginalName();
        // menggunakan model
        $update = Profile::where("id", $id)-> update([
            "phone" => $request["phone"],
            "gender" => $request["gender"],
            'photo' => $new_gambar,
            "user_id" => Auth::id()
        ]);
        $gambar->move('uploads/posts/',$new_gambar);
        return redirect('/user/profil');
    }
}