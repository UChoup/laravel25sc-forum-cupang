{{-- @extends('layouts.app') --}}
{{-- @extends('templatehome.masterhome') --}}
@extends('front.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <div class="card">
                <div class="card-header">{{ __('Pre Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="/preregister">
                        @csrf

                        <div class="shadow-none p-4 mb-4 bg-light rounded form-group">
                            <div class="shadow-none p-4 mb-4 bg-light rounded">
                                <p class="text-justify" name="register" style="width:600px; height:200px;" disabled>
                                    <small>
                                        Forum Rules<br>
                                        Registration to the forum is free! We have to make sure that you comply with the rules and policies which are strictly below. If you agree with the terms, check the 'I agree' box and hit the 'Register' button below.
                                        If you wish to cancel registration, <a class="text-danger" href="/">click here</a> to return to the homepage.
                                        Although administrators and moderators will endeavor to ban all inappropriate messages from this forum, it is not possible for us to review all messages. All messages are the views of the author, not the owner of this website.
                                        With this scheme, you guarantee that you will not post any message that is obscene, vulgar, sexually oriented, hateful, threatening, or violates any laws.
                                        The owner of this website reserves the right to measure, move, move or close any thread for any reason.
                                        <a class="text-danger" href="/terms" target="_blank">READ MORE...</a>
                                    </small>
                                </p>
                            </div>
                        </div>

                        <div class="form-group form-check">
                            <input type="hidden" name="check" value=0 />
                            <input class="form-check-input" type="checkbox" id="check" name="check" value=1 />
                            <label class="form-check-label col-sm-6 small" for="check" >
                                    I have read and agree to this rules.<br>
                            </label>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
