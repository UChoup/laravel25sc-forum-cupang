{{-- @extends('layouts.app') --}}
{{-- @extends('templatehome.masterhome') --}}
@extends('front.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <div class="card">
                <div class="card-header">{{ __('Pre Register') }}</div>

                <div class="card-body">


                        <div class="form-group row">
                            <p class="text-left"> You have chosen not to accept the forum rules, so registration cannot continue.
                                <a class="text-danger" href="/">Click here</a> to return to the main forums page, or click the 'Back' button on your browser if you now want to agree with the forum rules.
                            </p>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
