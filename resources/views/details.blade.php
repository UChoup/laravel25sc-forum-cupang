@extends('front.master')
@section('content')
<h1 class="mb-2 font-weight-bold"><i>{{$post->title}}</i></h1>

  {{-- <img src="{{asset('/ui/images/cupang-1.png')}}" alt="" class="img-fluid"> --}}
  <img src="/uploads/posting/{{$post->photo}}" width=500px height=200px class="img-fluid">

</p>
  {{$post->body}}
<p>

</p>

<div class="about-author d-flex">
  <div class="bio mr-4" style="width: 100px;">
    @if ($post->author->profile)
      <img src="/uploads/posts/{{$post->author->profile->photo}}" alt="Image placeholder" class="img-fluid mb-4">
    @else
      <img src="{{asset('adminlte/dist/img/default.png')}}" class="img-fluid mb-4" alt="User Image">
    @endif
  </div>
  <div class="desc">
    <h3>{{$post->author->name}}</h3>
  </div>
</div>


<div class="pt-2 mt-2">
  <ul class="comment-list">
    @forelse ($post->comments as $key => $comment)
      <li class="comment">
        <div class="vcard bio">
          @if ($comment->author->profile)
            <img src="/uploads/posts/{{$comment->author->profile->photo}}" alt="Image placeholder" class="img-fluid mb-4">
          @else
            <img src="{{asset('adminlte/dist/img/default.png')}}" class="img-fluid mb-4" alt="User Image">
          @endif
          {{-- <img src="{{asset('/ui/images/person_1.jpg')}}" alt="Image placeholder"> --}}
        </div>
        <div class="comment-body">
          <h3>{{$comment->author->name}}</h3>
          <div class="meta">{{$comment->created_at}}</div>
          <p>
            {{$comment->body}}
          </p>
        </div>
      </li>
    @empty
    <tr>
        <td colspan="4" align="center"> No Comments</td>
    </tr>
    @endforelse
  </ul>

      {{-- <ul class="children">
        <li class="comment">
          <div class="vcard bio">
            <img src="{{asset('/ui/images/person_1.jpg')}}" alt="Image placeholder">
          </div>
          <div class="comment-body">
            <h3>John Doe</h3>
            <div class="meta">October 03, 2018 at 2:21pm</div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
            <p><a href="#" class="reply">Reply</a></p>
          </div>
        </li>
      </ul>
    </li>
    <li class="comment">
      <div class="vcard bio">
        <img src="{{asset('/ui/images/person_1.jpg')}}" alt="Image placeholder">
      </div>
      <div class="comment-body">
        <h3>John Doe</h3>
        <div class="meta">October 03, 2018 at 2:21pm</div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
        <p><a href="#" class="reply">Reply</a></p>
      </div>
    </li>
  </ul> --}}
  <!-- END comment-list -->

  <div class="comment-form-wrap pt-2">
    <h3 class="mb-5">Leave a comment</h3>
    <form action="/comment" method="POST" class="p-3 p-md-5 bg-light">
        @csrf
        <input type="hidden" name="post" value="{{$post->id}}" />
      <div class="form-group">
        <label for="body">Comment</label>
        <textarea name="body" id="body" cols="30" rows="10" class="form-control"></textarea>
        @error('body')
        <div class="alert alert-danger">
            {{$message}}
        </div>
        @enderror
      </div>
      <div class="form-group">
        <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary">
      </div>

    </form>
  </div>
</div>
@endsection
