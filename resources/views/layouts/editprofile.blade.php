@extends('templateuser.masteruser')

@section('content')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
<div class="row flex-lg-nowrap">

  <div class="col">
    <div class="row">
      <div class="col mb-3">
        <div class="card">
          <div class="card-body">
            <div class="e-profile">
              <div class="tab-content pt-3">
                <div class="tab-pane active">

                  <form class="form" action='/user/profil/{{Auth::User()->id}}' method='post' enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                      <div class="col">
                        <div class="row">
                          <div class="col">
                          </div>
                          <div class="col">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                               <label><h3>Edit Profil Anda</h3></label> <br>
                              <label>Gender</label>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" name="gender" id="exampleRadios1" value="male" checked>
                                <label class="form-check-label" for="exampleRadios1">
                                Male
                                </label>
                                </div>
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="gender" id="exampleRadios2" value="female">
                                <label class="form-check-label" for="exampleRadios2">
                                Female
                                </label>
                            </div>
                            </div>
                            <div class="form-group">
                              <label>Phone</label>
                              <input class="form-control" type="text" name="phone">
                            </div>
                            <div class="form-group">
                            <label for="exampleFormControlFile1">Masukkan Photo Profil</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1" name = 'img'>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col d-flex justify-content-end">
                        <button class="btn btn-primary" type="submit">Save Changes</button>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
</div>
@endsection
