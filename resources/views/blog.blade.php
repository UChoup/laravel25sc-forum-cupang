@extends('front.master')
@section('content')

@forelse ($postlist as $key => $post)
    <div class="row">
        <div class="col-md-12">
            <div class="blog-entry ftco-animate">
                <img src="/uploads/posting/{{$post->photo}}" width=500px height=200px>
                <div class="text pt-3">
                    <h3 class="mb-2"><a href="/details/{{$post->id}}"><span>{{$post->title}}</span></a></h3>
                    <div class="meta-wrap">
                        <p class="meta">
                            <span>{{$post->author->name}}</span>
                            <span>{{$post->created_at}}</span>
                        </p>
                    </div>
                    {{-- <a href="/details/{{$post->id}}">Details</a> --}}
                    <p><a href="/details/{{$post->id}}" class="btn btn-primary">Details <span class="ion-ios-arrow-forward"></span></a></p>
                </div>
            </div>
        </div>
    </div>
    @empty
<tr>
    <td colspan="4" align="center"> No Data</td>
</tr>
@endforelse

<div class="row">
    <div class="col">
        <div class="block-27">
            <ul>
                <li><a href="#">&lt;</a></li>
                <li class="active"><span>1</span></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&gt;</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection()
