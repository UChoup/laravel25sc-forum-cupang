<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{asset('home/img/logo/FORUM _ CUPANG _.png')}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Forum Ikan Cupang</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
                @if (Auth::User()->profile)
                    <img src="/uploads/posts/{{Auth::User()->profile->photo}}" class="img-circle elevation-2" alt="User Image">
                @else
                    <img src="{{asset('adminlte/dist/img/default.png')}}" class="img-circle elevation-2" alt="User Image">
                @endif
        </div>
        <div class="info">
          <a href="/user/profil" class="d-block">{{Auth::User()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Home
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="/post" class="nav-link">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                Postingan Blog
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/user/profil" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Profile
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
