@extends('templateuser.masteruser')
@section('content')
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Data Postingan</h3>
      </div>
      <div>
        <a class="btn btn-success sm m-2" href="/post/create">Create Data</a>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#No</th>
            <th>Title</th>
            <th>Content</th>
            <th>Photo</th>
            <th>Action</th>
          </tr>
          </thead>
            <tbody>
                @forelse($post as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->title}}
                    </td>
                    <td>{{$value->body}}</td>
                    <td><img style="width: 100px;" src="{{asset('/uploads/posting/'.$value->photo)}}"></td>
                    <td>
                        <form action="/post/{{$value->id}}" method="POST">
                            <a href="/post/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/post/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            @csrf
                            @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
                @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
                @endforelse
            </tbody>
          <tfoot>
          <tr>
            <th>#No</th>
            <th>Title</th>
            <th>Content</th>
            <th>Photo</th>
            <th>Action</th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@endsection

@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

