@extends('templateuser.masteruser')
@section('content')
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Posting Blog</h3>
              </div>
              <div>
                <a class="btn btn-danger sm m-2" href="/post">Back</a>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="/post" method="POST" enctype="multipart/form-data">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Masukan title">
                    @error('title')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="body">Body</label>
                    <textarea name="body" id="body" class="form-control" cols="30" rows="10"></textarea>
                    @error('body')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="img">Upload Image</label>
                    <input type="file" class="form-control" name="img">
                    @error('img')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
              </form>
            </div>
@endsection
