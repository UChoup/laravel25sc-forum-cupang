@extends('templateuser.masteruser')
@section('title', 'Show Data Posting')
@section('content')
<div class="nav nav justify-content-center mt-2">
    <div class="card" style="width: 500px;">
        <img src="{{asset('/uploads/posting/'.$post->photo)}}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title mb-2"><b>{{$post->title}}</b></h5>
            <p class="card-text">{{$post->body}}</p>
            <a href="/post" class="btn btn-danger">Back</a>
        </div>
    </div>
</div>
@endsection
