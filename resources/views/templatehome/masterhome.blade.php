<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>News  HTML-5 Template </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="home/img/favicon.ico">

    <!-- CSS here -->
    <link rel="stylesheet" href="home/css/bootstrap.min.css">
    <link rel="stylesheet" href="home/css/owl.carousel.min.css">
    <link rel="stylesheet" href="home/css/ticker-style.css">
    <link rel="stylesheet" href="home/css/flaticon.css">
    <link rel="stylesheet" href="home/css/slicknav.css">
    <link rel="stylesheet" href="home/css/animate.min.css">
    <link rel="stylesheet" href="home/css/magnific-popup.css">
    <link rel="stylesheet" href="home/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="home/css/themify-icons.css">
    <link rel="stylesheet" href="home/css/slick.css">
    <link rel="stylesheet" href="home/css/nice-select.css">
    <link rel="stylesheet" href="home/css/style.css">
</head>

<body>
<!-- Preloader Start -->
{{-- @include('template.partials.preloader') --}}
<!-- Preloader Start -->

<!-- Header Start -->
@include('templatehome.partials.header')
<!-- Header End -->

<main>
    <!-- Whats New Start -->
    <section class="whats-news-area pt-50 pb-20 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                <div class="whats-news-wrapper">
                    <!-- Tab content -->
                    <div class="row">
                        <div class="col-12">
                            <!-- Nav Card -->
                            <div class="tab-content" id="nav-tabContent">
                                <!-- card one -->
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="row">
                                        @yield('content')
                                        {{-- MULAI ISI CONTENT NYA DISINI --}}
                                        {{-- <!-- Left Details Caption -->
                                        <div class="col-xl-6 col-lg-12">
                                            <div class="whats-news-single mb-40 mb-40">
                                                <div class="whates-img">
                                                    <img src="home/img/gallery/whats_news_details1.png" alt="">
                                                </div>
                                                <div class="whates-caption">
                                                    <h4><a href="latest_news.html">Secretart for Economic Air plane that looks like</a></h4>
                                                    <span>by Alice cloe   -   Jun 19, 2020</span>
                                                    <p>Struggling to sell one multi-million dollar home currently on the market won’t stop actress and singer Jennifer Lopez.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Left Details Caption -->
                                        <div class="col-xl-6 col-lg-12">
                                            <div class="whats-news-single mb-40 mb-40">
                                                <div class="whates-img">
                                                    <img src="home/img/gallery/whats_news_details1.png" alt="">
                                                </div>
                                                <div class="whates-caption">
                                                    <h4><a href="latest_news.html">Secretart for Economic Air plane that looks like</a></h4>
                                                    <span>by Alice cloe   -   Jun 19, 2020</span>
                                                    <p>Struggling to sell one multi-million dollar home currently on the market won’t stop actress and singer Jennifer Lopez.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Left Details Caption -->
                                        <div class="col-xl-6 col-lg-12">
                                            <div class="whats-news-single mb-40 mb-40">
                                                <div class="whates-img">
                                                    <img src="home/img/gallery/whats_news_details1.png" alt="">
                                                </div>
                                                <div class="whates-caption">
                                                    <h4><a href="latest_news.html">Secretart for Economic Air plane that looks like</a></h4>
                                                    <span>by Alice cloe   -   Jun 19, 2020</span>
                                                    <p>Struggling to sell one multi-million dollar home currently on the market won’t stop actress and singer Jennifer Lopez.</p>
                                                </div>
                                            </div>
                                        </div> --}}
                                        {{-- BERHENTI ISI CONTENT NYA DISINI --}}
                                    </div>
                                </div>
                        <!-- End Nav Card -->
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Whats New End -->
</main>

<!-- Footer Start -->
@include('templatehome.partials.footer')
<!-- Footer End -->

<!-- Search model Begin -->
<div class="search-model-box">
    <div class="d-flex align-items-center h-100 justify-content-center">
        <div class="search-close-btn">+</div>
        <form class="search-model-form">
            <input type="text" id="search-input" placeholder="Searching key.....">
        </form>
    </div>
</div>
<!-- Search model end -->

<!-- JS here -->

    <script src="./home/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="./home/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="./home/js/popper.min.js"></script>
    <script src="./home/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="./home/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="./home/js/owl.carousel.min.js"></script>
    <script src="./home/js/slick.min.js"></script>
    <!-- Date Picker -->
    <script src="./home/js/gijgo.min.js"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="./home/js/wow.min.js"></script>
    <script src="./home/js/animated.headline.js"></script>
    <script src="./home/js/jquery.magnific-popup.js"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="./home/js/jquery.scrollUp.min.js"></script>
    <script src="./home/js/jquery.nice-select.min.js"></script>
    <script src="./home/js/jquery.sticky.js"></script>

    <!-- contact js -->
    <script src="./home/js/contact.js"></script>
    <script src="./home/js/jquery.form.js"></script>
    <script src="./home/js/jquery.validate.min.js"></script>
    <script src="./home/js/mail-script.js"></script>
    <script src="./home/js/jquery.ajaxchimp.min.js"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="./home/js/plugins.js"></script>
    <script src="./home/js/main.js"></script>
    
    @include('sweetalert::alert')

</body>
</html>
