{{-- @extends('templatehome.masterhome') --}}
@extends('front.master')

@section('content')
    <main>
    <div class="about-details section-padding30">
        <div class="container">
            <div class="row">
                <div class="offset-xl-1 col-lg-8">
                    <div class="about-details-cap mb-50">
                        <h4>Our Mission</h4>
                        <p>Consectetur adipiscing elit, sued do eiusmod tempor ididunt udfgt labore et dolore magna aliqua. Quis ipsum suspendisces gravida. Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan lacus. Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan lacus.
                            </p>
                        <p> Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan lacus. Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan.</p>
                    </div>
                    <div class="about-details-cap mb-50">
                        <h4>Our Vision</h4>
                        <p>Consectetur adipiscing elit, sued do eiusmod tempor ididunt udfgt labore et dolore magna aliqua. Quis ipsum suspendisces gravida. Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan lacus. Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan lacus.
                            </p>
                        <p> Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan lacus. Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Team Start -->
    <div class="team-area section-padding30">
        <div class="container">
            <div class="row">
                <div class="cl-xl-7 col-lg-8 col-md-10">
                    <!-- Section Tittle -->
                    <div class="section-tittles mb-70">
                        <span>Our Professional Developer </span>
                        <h2>Our Team</h2>
                    </div>
                </div>
            </div>
            <div>
                <!-- single Tem -->
                <div>
                    <div class="single-team mb-30">
                        <div class="team-img">
                            <img src="home/img/gallery/y.JPG" alt="">
                        </div>
                        <div class="team-caption">
                            <h3><a href="#">Yusuf Wijaya</a></h3>
                            <span>WEB Developer</span>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="single-team mb-30">
                        <div class="team-img">
                            <img src="home/img/gallery/team3.png" alt="">
                        </div>
                        <div class="team-caption">
                            <h3><a href="#">Kusnan</a></h3>
                            <span>WEB Developer</span>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="single-team mb-30">
                        <div class="team-img">
                            <img src="home/img/gallery/team1.png" alt="">
                        </div>
                        <div class="team-caption">
                            <h3><a href="#">Putra Pradana</a></h3>
                            <span>WEB Developer</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Team End -->

    <!-- banner-last End -->
</main>
@endsection
