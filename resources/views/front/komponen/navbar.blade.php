<aside id="colorlib-aside" role="complementary" class="js-fullheight">
    <nav id="colorlib-main-menu" role="navigation">
        <ul>
               <li class="nav-item {{ request()->is('/') ? 'colorlib-active' : ''}}"><a href="/">Home</a></li>
            <li class="nav-item {{ request()->is('about') ? 'colorlib-active' : ''}}"><a href="/about">About Us</a></li>
                <!-- Authentication Links -->
            @guest
                <li class="nav-item {{ request()->is('login') ? 'colorlib-active' : ''}}">
                    <a class="nav-item " href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item {{ request()->is('preregister') ? 'colorlib-active' : ''}}">
                        <a class="nav-item" href="/preregister">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item ml-3" href="/user/profil">
                            Profile
                        </a>
                        <a class="dropdown-item ml-3" href="/post">
                            Post
                        </a>

                        <a class="dropdown-item ml-3" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                                {{-- Logout --}}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </nav>

    <div class="colorlib-footer">
        <h1 id="colorlib-logo"><a href="/">FIC</a></h1>
    </div>
</aside>
