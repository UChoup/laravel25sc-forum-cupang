<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Arahkan ke home
// Route::get('/', function () {
//     return view('templatehome.masterhome');
// });

// Arahkan ke About
Route::get('/about', function () {
    return view('templatehome.masterabout');
});

// Arahkan ke User
Route::get('/user', function () {
    return view('templateuser.masteruser');
})->middleware('auth');

// bagian profile
Route::get('/user/profil', 'profilController@check');
Route::put('/user/profil/{id}', 'profilController@update');
Route::post('/user/profil', 'profilController@store');
Route::get('/user/profil/{id}/edit', 'profilController@edit');
// Route::put('/user/profil/{id}', 'profilController@update');

Auth::routes();

//to pre register with terms and condition
Route::get('/preregister', 'Auth\PreRegisterController@preregister');

//if checkbox is checked then next to register else failed
Route::post('/preregister', 'Auth\PreRegisterController@check');

Route::get('/terms', 'PdfController@generatePdf');

// //Tampilan Baru
// Route::get('/', function () {
//     return view('blog');
// });
// Route::get('/details', function () {
//     return view('details');
// });

//Post blog
Route::resource('post', 'PostController')->middleware('auth');

Route::resource('comment', 'CommentController')->middleware('auth');

Route::get('/', 'PostController@getAll');

Route::get('/details/{post_id}', 'PostController@editAll')->name('detailpost');

