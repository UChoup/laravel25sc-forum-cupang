<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


## FINAL PROJECT 

## KELOMPOK 21
- Kusnantoro
- Muhammad Putra Pradana
- Yusuf Wijaya


## TEMA
Website Forum Pecinta Ikan Cupang

## LINK VIDEO DEMO
- Google Drive : https://drive.google.com/drive/folders/1DIGGmr7BzcCrb1zrOpGYapXBoeq0RkmZ?usp=sharing

## LINK DEPLOY
https://forumpecintacupang.herokuapp.com/

## ERD
Terlampir di folder /public/erd/ForumPecintaIkanCupang.png
